﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace combustion_engine
{
    public class Engine : IEngine
    {
        public class Settings {
            public float I;
            public List<float> arrayM { get; set; }
            public List<float> arrayV { get; set; }
            public float T_critical;
            public float Hm;
            public float Hv;
            public float C;
        }

        public class State {
            public float TemperatureEnvironment = 0f;
            public float Velocity = 0f;
            public float TemperatureEngine = 0f;
        }

        Settings settings;
        State state;

        public Engine(Settings settings)
        {
            this.settings = settings;
            this.state = new State();
        }

        public void Reset(float temperatureEnvironment) {
            state.Velocity = 0f;
            state.TemperatureEngine = temperatureEnvironment;
            state.TemperatureEnvironment = temperatureEnvironment;
        }

        public void UpdateState(float deltaTime)
        {
            CalculateVelocity(deltaTime);
            CalculateTemperature(deltaTime);
        }

        public bool IsEngineRunhot()
        {
            bool flag = false;

            if (state.TemperatureEngine >= settings.T_critical)
            {
                flag = true;
            }

            return flag;
        }

        public float GetTemperature() {
            return state.TemperatureEngine;
        }

        void CalculateVelocity(float deltaTime)
        {
            state.Velocity += deltaTime * Accelerate();
        }

        void CalculateTemperature(float deltaTime)
        {
            state.TemperatureEngine += deltaTime * (CalculateVH() - CalculateVC());
        }

        float Accelerate()
        {
            float M = CalculateM(state.Velocity);
            return M / settings.I;
        }

        float CalculateVH()
        {
            float M = CalculateM(state.Velocity);
            return (M * settings.Hm + MathF.Pow(state.Velocity, 2) * settings.Hv);
        }

        float CalculateVC()
        {
            return settings.C * (state.TemperatureEnvironment - state.TemperatureEngine);
        }

        float CalculateM(float f_V)
        {
            float M = 0f;

            int limitLeft = 0;
            int limitRight = settings.arrayV.Count - 1;
            int selection = (limitRight - limitLeft) / 2;

            SearchSectionGraphicV(f_V, ref selection, ref limitLeft, ref limitRight);

            float valueLeftV = settings.arrayV[limitLeft];
            float valueRightV = settings.arrayV[limitRight];

            float normValueInSection = (f_V - valueLeftV) / (valueRightV - valueLeftV);

            float valueLeftM = settings.arrayM[limitLeft];
            float valueRightM = settings.arrayM[limitRight];

            M = valueLeftM + normValueInSection * (valueRightM - valueLeftM);

            return M;
        }


        void SearchSectionGraphicV(float value, ref int selection, ref int limitLeft, ref int limitRight)
        {
            if (value < settings.arrayV[selection])
            {
                limitRight = selection;
            }
            else
            {
                limitLeft = selection;
            }

            if (limitRight - limitLeft > 1)
            {
                int mediana = limitLeft + (limitRight - limitLeft) / 2;
                selection = mediana;
                SearchSectionGraphicV(value, ref selection, ref limitLeft, ref limitRight);
            }
        }
    }
}