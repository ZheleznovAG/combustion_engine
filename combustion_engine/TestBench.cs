﻿using Newtonsoft.Json;
using System;

namespace combustion_engine
{
    public class TestBench
    {
        public class Settings
        {
            public float DELTA_TIME = .01f;
            public float TIME_BREAK_TEST = 10000f;
        }
        public class State
        {
            internal float timeProcess = 0f;
            internal bool isDone = false;
            internal float temperatureEnvironment = 0f;
        }

        IEngine engine;
        State state;
        Settings settings;

        public TestBench(Settings settings)
        {
            state = new State();
            this.settings = settings;
        }

        /// <summary>
        /// Attach engine
        /// </summary>
        /// <param name="engine"></param>
        public void SetEngine(IEngine engine)
        {
            this.engine = engine;
        }

        /// <summary>
        /// Execute test with return log
        /// </summary>
        /// <param name="temperatureEnvironment"></param>
        public string ExecuteTest(float temperatureEnvironment)
        {
            string result = "";

            if (engine == null)
            {

                return "0";
            }
            Reset(temperatureEnvironment);
            engine.Reset(temperatureEnvironment);
            while (state.timeProcess < settings.TIME_BREAK_TEST)
            {
                state.timeProcess += settings.DELTA_TIME;
                engine.UpdateState(settings.DELTA_TIME);

                if (engine.IsEngineRunhot())
                {
                    break;
                }
            }

            result = "Time execution test: " + state.timeProcess + " second(s)";

            return result;
        }

        void Reset(float temperatureEnvironment)
        {
            state.isDone = false;
            state.timeProcess = 0f;
            state.temperatureEnvironment = temperatureEnvironment;
        }
    }
}