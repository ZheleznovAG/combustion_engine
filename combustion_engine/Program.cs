﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace combustion_engine
{
    class Program
    {
        static class Settings
        {
            public static float ABSOLUTE_ZERO_CELSIUS = -273.15F;
            public static float TEMPERATURE_MAX_LIMIT = 1E+4F;
        }

        static string NAME_FILE_SETTINGS_ENGINE = "settingsEngine.json";
        static string NAME_FILE_SETTINGS_TESTBENCH = "settingsBench.json";

        static void Main(string[] args)
        {
            string arg = TryGetFirstArg(args);
            float temperatureEnvironment = TryParseValueTemperature(arg);

            LoaderSettings loaderSettings = new LoaderSettings();

            TestBench.Settings settingsBench = loaderSettings.GetSettings<TestBench.Settings>(NAME_FILE_SETTINGS_TESTBENCH);
            Engine.Settings settingsEngine = loaderSettings.GetSettings<Engine.Settings>(NAME_FILE_SETTINGS_ENGINE);

            string result = "";
            bool isAvaibleSettingsBench = (settingsBench != null);
            bool isAvaibleSettingsEngine = (settingsEngine != null);

            if (isAvaibleSettingsBench && isAvaibleSettingsEngine)
            {
                TestBench testBench = new TestBench(settingsBench);
                Engine engine = new Engine(settingsEngine);

                testBench.SetEngine(engine);

                result = testBench.ExecuteTest(temperatureEnvironment);

                Console.WriteLine(result);
            }
        }

        static string TryGetFirstArg(string[] args)
        {
            string str = "";

            if (args != null && args.Length > 0)
            {
                str = args[0];
            }

            return str;
        }

        static float TryParseValueTemperature(string str) {
            float value;

            bool flag = float.TryParse(str, out value);

            if (flag == false)
            {
                Console.WriteLine("Error parse input value temperature environment, set default value 0");
            }

            value = ClampValueTemperature(value);

            return value;
        }

        static float ClampValueTemperature(float value)
        {
            return Math.Clamp(value, Settings.ABSOLUTE_ZERO_CELSIUS, Settings.TEMPERATURE_MAX_LIMIT);
        }

    }
}
