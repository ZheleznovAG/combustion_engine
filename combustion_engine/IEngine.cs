﻿namespace combustion_engine
{
    public interface IEngine
    {
        /// <summary>
        /// Restart state engine
        /// </summary>
        /// <param name="temperatureEnvironment"></param>
        void Reset(float temperatureEnvironment);

        /// <summary>
        /// Execute iteration simulation
        /// </summary>
        /// <param name="deltaTime"></param>
        void UpdateState(float deltaTime);

        /// <summary>
        /// Check engine runhot
        /// </summary>
        /// <returns></returns>
        bool IsEngineRunhot();

        /// <summary>
        /// Get current temperature engine
        /// </summary>
        /// <returns></returns>
        float GetTemperature();
    }
}