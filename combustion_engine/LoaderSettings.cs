﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace combustion_engine
{
    public class LoaderSettings
    {
        public T GetSettings<T>(string path)
        {
            return CreateSettings<T>(path);
        }

        static T CreateSettings<T>(string path)
        {
            T value = default(T);

            string str = ReadFile(path);

            value = ParseDataFile<T>(str);

            return value;
        }

        static T ParseDataFile<T>(string data)
        {
            T value = default(T);

            value = JsonConvert.DeserializeObject<T>(data);

            return value;
        }

        static string ReadFile(string path)
        {
            string str = "";

            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        str = sr.ReadToEnd();
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            return str;
        }
    }
}